package net.openu.queue.concurrency.service;


import net.openu.queue.concurrency.entity.Message;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{
	private BlockingQueue<Message> queue;

	public Consumer (BlockingQueue<Message> q){
		this.queue =q ;
	}
	@Override
	public void run() {
		try{

			Message msg;
			while (!"exit".equalsIgnoreCase((msg = queue.take()).getMsg())){
				Thread.sleep(10);
				System.out.println("Consumed "+msg.getMsg());
		//		System.out.println(queue.size());
			}
		}catch (InterruptedException e){
			e.printStackTrace();
		}
	}
}
