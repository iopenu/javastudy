package net.openu.queue;

import java.util.LinkedList;
import java.util.Queue;

public class SimpleQueueMain {
	public static void main(String[] args) {
		Queue<String> q1 = new LinkedList<String>();
		System.out.println(">"+ Integer.MIN_VALUE);
		q1 .offer("Dog");   //enqueue
		q1 .offer("Cat");
		q1 .offer("Chicken");
		System.out.println("q1 size >"+q1.size());
		System.out.println(q1.poll()); //dequeue;
		System.out.println("q1 size >"+q1.size());
		System.out.println("q1 > "+ q1.peek()); //꺼내는 대상을 리턴한다.

	}
}
