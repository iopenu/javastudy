package net.openu.queue.threadPool;

import java.util.LinkedList;

public class ThreadPoolQueue {
	//Item을 저장할 큐 선언
	private LinkedList<Object> queue = new LinkedList<Object>();

	//queue 최대 사이즈
	private int MAX_QUEUE_SIZE = 5;

	//콘솔출력변수
	private boolean DEBUG = false;

	public synchronized void enqueue(Object item) throws InterruptedException{
		while(queue.size() == MAX_QUEUE_SIZE){
			console("enqueue waiting...");
			wait();
		}

		if(queue.size() == 0){
			console("enqueue notifyall ...");
			notifyAll();
		}

		console("enqueue adding ...");
		queue.add(item);
	}

	public synchronized Object dequeue() throws InterruptedException{
		if(queue.size() == 0){
			console("dequeue waiting...");
			wait();
		}

		if( queue.size() == MAX_QUEUE_SIZE ){
			console("dequeue notifyall...");
			notifyAll();
		}

		console("dequeue removing...");
		return queue.remove(0);

	}

	public ThreadPoolQueue(int MAX_QUEUE_SIZE){
		this.MAX_QUEUE_SIZE = MAX_QUEUE_SIZE;
	}

	// 디버깅 설정
	public void toggleDebug(boolean flag){
		this.DEBUG = flag;
	}

	public void console(String msg){
		if(DEBUG)
			System.out.println(msg);
	}
}
