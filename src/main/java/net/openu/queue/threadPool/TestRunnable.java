package net.openu.queue.threadPool;

// 테스트할 용도의 Runnable 클래스
class TestRunnable implements Runnable{
	int index;
	public TestRunnable(int i) {
		index = i;
	}
	@Override
	public void run() {
		System.out.println("[index]"+index);
	}

}