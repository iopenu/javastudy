package net.openu.queue.threadPool;

public class TestMain {

	public static void main(String[] args) {
		ThreadPool p = new ThreadPool(5,10);
		p.toggleDebugWithQueue(true); // 아이템 큐 콘솔 디버깅 ON
		p.toggleDebugWithRunnable(true); // 쓰레드 콘솔 디버깅 ON
		try {

			// 아이템 100개를 쓰레드풀에 삽입 및 실행
			for(int i = 0 ; i < 100; i++){
				Runnable r = new TestRunnable(i);
				p.execute(r);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}


		// 아이템이 처리됐든 안됐든 일단 쓰레드풀 정지
		try {
			p.stop();
			System.out.println("stop!!!");
		} catch (InterruptedException e) {
			System.out.println("ThreadPool is stopped.");
		}

	}

}


