package net.openu.queue.implemenation;

public class Queue {
	int front,rear,size;
	int capacity;  //전체크기
	int[] array;

	//
	public Queue(int capacity){
		this.capacity = capacity;
		front  = this.size = 0;
		rear  = capacity -1;
		array = new int[this.capacity];
	}

	public boolean isFull(){
		return (this.size == this.capacity);
	}

	public boolean isEmpty(){
		boolean returnVal = false;
		if(this==null || this.size==0) returnVal = true;
		return returnVal;
	}

	public int enQueue(int entity){
		int resultVal = -999999999;
		if(isFull()){
			System.out.println("queue is full");
		}else {
			this.rear = (this.rear +1) % this.capacity;
			this.array[this.rear] = entity;
			this.size++;
			System.out.println("["+entity+"] enqueue to queue");
			resultVal = this.size;
		}
		return resultVal;
	}
	public int deQueue(){
		int resultVal = -999999999;
		if(isEmpty()){
			System.out.println("queue is empty");
		}else{
			resultVal = this.array[this.front];
			this.front = (this.front+1)%this.capacity;
			this.size--;
			System.out.println("["+resultVal+"] dequeue to queue");
		}
		return resultVal;
	}

	public int peek(){
		int resultVal = 0;
		if(isEmpty()){
			resultVal  = -999999999;
		}else{
			resultVal = this.array[this.front];
		}
		return resultVal;
	}

	public int rear(){
		int resultVal = 0;
		if(isEmpty()){
			resultVal  = -999999999;
		}else{
			resultVal = this.array[this.rear];
		}
		return resultVal;
	}



}
