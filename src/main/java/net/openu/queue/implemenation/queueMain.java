package net.openu.queue.implemenation;

public class queueMain {
	public static void main(String[] args) {
		Queue queue = new Queue(10000);
		queue.enQueue(1);
		queue.enQueue(2);
		queue.enQueue(3);
		queue.enQueue(4);
		queue.enQueue(5);


		System.out.println(queue.deQueue() + " dequeued from queue\n");
		System.out.println("Front item is " + queue.peek());
		System.out.println("Rear item is " + queue.rear());

		System.out.println("================================================");
		queue.enQueue(6);

		System.out.println(queue.deQueue() + " dequeued from queue\n");
		System.out.println("Front item is " + queue.peek());
		System.out.println("Rear item is " + queue.rear());

		while (!queue.isEmpty()){
			System.out.println(queue.deQueue() + " dequeued from queue\n");
		}
	}
}
