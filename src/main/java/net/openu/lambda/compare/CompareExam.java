package net.openu.lambda.compare;

public class CompareExam {
	public static void exec(Compare compare){
		int k = 10;
		int u = 20;
		int value = compare.compareTo(k,u);
		System.out.println(value);
	}

	public static void main(String[] args) {
		exec((i,j) ->{
			return i-j;
		});
	}
}
