package net.openu.lambda.compare;

public interface Compare {
	int compareTo(int value1,int value2);
}
