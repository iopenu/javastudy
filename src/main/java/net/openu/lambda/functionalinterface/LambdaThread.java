package net.openu.lambda.functionalinterface;

public class LambdaThread {

	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("일회용 스레드");
			}
		}).start();

		new Thread(()->{
			System.out.println("람다표현식을 사용한 일회용 스레드 생성");
		}).start();
	}
}
