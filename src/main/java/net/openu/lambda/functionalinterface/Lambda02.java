package net.openu.lambda.functionalinterface;

public class Lambda02 {

	@FunctionalInterface
	interface Calc {
		int min(int x,int y);
	}


	public static void main(String[] args) {
		Calc minNum = ((x, y) -> x<y? x:y);     //추상메소드 구현
		System.out.println(minNum.min(3,4));
	}
}
