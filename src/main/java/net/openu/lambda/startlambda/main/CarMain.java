package net.openu.lambda.startlambda.main;

import net.openu.lambda.startlambda.entity.Car;
import net.openu.lambda.startlambda.util.CarUtil;

import java.util.ArrayList;
import java.util.List;

public class CarMain {
	public static void main(String[] args) {
		List<Car> cars = new ArrayList<>();
		cars.add(new Car("경차",2,800,2));
		cars.add(new Car("봉고",9,1000,1));
		cars.add(new Car("소형",5,2200,0));
		cars.add(new Car("중형",5,3300,3));

		//printCarCheaperThan(cars,2000);

		CarUtil carUtils = new CarUtil();
		carUtils.printCar(cars,(Car car)-> {
			return car.capacity >=4 &&car.price <2000;
		}
		);

	}



//	public static void printCarCheaperThan(List<Car> cars,int price){
//		for(Car car :cars){
//			if(car.price<price){
//				System.out.println(car);
//			}
//		}
//	}


}
