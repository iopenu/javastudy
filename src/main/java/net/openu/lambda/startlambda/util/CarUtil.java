package net.openu.lambda.startlambda.util;

import net.openu.lambda.startlambda.entity.Car;
import net.openu.lambda.startlambda.service.CheckCar;

import java.util.List;

public class CarUtil {
	public void printCar(List<Car> cars , CheckCar tester){
		for(Car car: cars){
			if(tester.test(car)){
				System.out.println(car);
			}
		}
	}
}
