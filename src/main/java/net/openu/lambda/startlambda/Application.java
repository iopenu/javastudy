package net.openu.lambda.startlambda;

interface Say{
	void someThing(int a,int b);
}

interface Hello{
	void something(String a,String b);	//functional interface
}

class Person{
	public void greeting(Say line){
		line.someThing(3,5);
	}

	public void greeting(Hello line){
		line.something("hello","world");
	}
}

public class Application {
	public static void main(String[] args) {
		Person baek = new Person();

		baek.greeting(new Say() {
			@Override
			public void someThing(int a,int b) {
				System.out.println("hi");
				System.out.println("paramer value is "+ a);

			}
		});

		System.out.println("====================");

		baek.greeting((String a,String b)->{
			System.out.println("hihi~");
			System.out.println("parameter value is "+ a);

		});
	}
}
