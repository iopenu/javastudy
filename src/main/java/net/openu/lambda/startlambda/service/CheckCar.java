package net.openu.lambda.startlambda.service;

import net.openu.lambda.startlambda.entity.Car;

public interface CheckCar {
	boolean test(Car car);
}
