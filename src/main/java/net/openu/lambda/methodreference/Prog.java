package net.openu.lambda.methodreference;

import java.util.function.DoubleUnaryOperator;

public class Prog {

	@FunctionalInterface
	interface Func {
		int cal(int a,int b);
	}

	public static void main(String[] args) {
		//DoubleUnaryOperator 인터페이스는 한 개의 double 형 매개변수를 전달받아 한 개의 double 형 값을 반환하는 java.util.function 패키지에서 제공하는 함수형 인터페이스입니다.
		DoubleUnaryOperator oper;
		oper = (n) -> Math.abs(n);          //람다 표현식
		System.out.println(oper.applyAsDouble(-5));

		oper = Math::abs;                   //메소드 참조
		System.out.println(oper.applyAsDouble(-5));

		Func sub = (int a,int b) -> a-b;
		Func add = (int a,int b)-> {return a+b;};
		Func mul = (int a,int b)-> a*b;

		int result = add.cal(3,4);
		int result2 = sub.cal(3,5);
		System.out.println(result);
		System.out.println(result2);

	}
}
