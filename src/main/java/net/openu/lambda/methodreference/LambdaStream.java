package net.openu.lambda.methodreference;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Collectors;

public class LambdaStream {
	public static void main(String[] args) {

		//foreach
		Arrays.asList(1,2,3).stream().forEach(System.out::println);
		System.out.println("============================================");
		Arrays.asList(1,2,3).parallelStream().forEach(System.out::println);      //병렬
		System.out.println("============================================");

		//map
		Arrays.asList(1,2,3).stream().map(i ->i*i).forEach(System.out::println);
		System.out.println("============================================");

		//limit
		Arrays.asList(1,2,3).stream().limit(2).forEach(System.out::println);
		System.out.println("============================================");

		//Skip
		Arrays.asList(1,2,3).stream()
				.skip(2)
				.forEach(System.out::println);
		System.out.println("============================================");

		//Filter
		Arrays.asList(1,2,3).stream()
				.filter(i -> i<=2)
				.forEach(System.out::println);
		System.out.println("============================================");

		//flatMap
		Arrays.asList(
			Arrays.asList(1,2),Arrays.asList(3,4,5),Arrays.asList(6,7,8,9)).stream().flatMap(i -> i.stream()
		).forEach(System.out::println);
		System.out.println("============================================");

		//reduce
		int reduceResult = 	Arrays.asList(1,2,3).stream()
					.reduce((a,b)-> a-b)
					.get();
		System.out.println(reduceResult);
		System.out.println("============================================");

		//collection
		Collection<Integer> a = Arrays.asList(1,2,3).stream().collect(Collectors.toList());
		Iterator<Integer> b = Arrays.asList(1,2,3).stream().iterator();

	}
}
