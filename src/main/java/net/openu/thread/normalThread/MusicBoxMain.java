package net.openu.thread.normalThread;

public class MusicBoxMain {
	public static void main(String[] args) {
		MusicBox box = new MusicBox();
		MusicPlayer hi1 = new MusicPlayer(1,box);
		MusicPlayer hi2 = new MusicPlayer(2,box);
		MusicPlayer hi3 = new MusicPlayer(3,box);

		hi1.start();
		hi2.start();
		hi3.start();


	}
}
