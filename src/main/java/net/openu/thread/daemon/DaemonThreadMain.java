package net.openu.thread.daemon;

import static java.lang.Thread.sleep;

public class DaemonThreadMain {
	public static void main(String[] args) {
		Thread th = new Thread(new DaemonThread());
		th.setDaemon(true);
		th.start();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("메인 쓰레드가 종료됩니다.");
	}

}
