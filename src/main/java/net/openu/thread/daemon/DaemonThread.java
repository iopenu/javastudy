package net.openu.thread.daemon;

public class DaemonThread implements Runnable {


	@Override
	public void run() {
		while (true){
			System.out.println("데몬 쓰레드가 실행중입니다.");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}
	}
}
