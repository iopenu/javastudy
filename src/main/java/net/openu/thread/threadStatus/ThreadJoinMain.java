package net.openu.thread.threadStatus;

public class ThreadJoinMain {
	public static void main(String[] args) {
		ThreadJoin thread = new ThreadJoin();
		thread.start();

		System.out.println("시작");
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("종료");
	}
}
