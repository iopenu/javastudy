package net.openu.thread.threadStatus;

public class ThreadWaitMain {
	public static void main(String[] args) {
		ThreadWait thread = new ThreadWait();
		thread.start();
		synchronized (thread){
			try {
				System.out.println("thread가 완료될때까지 기다립니다.");
				thread.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("thread total  >"+ thread.total);
		}
	}
}
